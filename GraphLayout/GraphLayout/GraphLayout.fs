﻿namespace Grafalgo

type Node<'TKey,'TValue> =
    'TKey *
    'TValue

type Edge<'TKey> =
    'TKey *
    'TKey *
    int64

type Atom<'TKey, 'TValue> =
    Node<'TKey, 'TValue> *
    Edge<'TKey> list

type Graph<'TKey, 'TValue> =
    Atom<'TKey, 'TValue> list


