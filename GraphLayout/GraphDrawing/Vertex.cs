﻿using System;
using System.Collections.Generic;

namespace GraphDrawing
{
    public class Vertex<T>
    {
        public T Identifier { get; private set; }
        public int X { get; private set; }
        public int Y { get; private set; }
        public int Width { get; private set; }
        public int Height { get; private set; }

        public Dictionary<Vertex<T>, Edge<T>> ConnectedVertices { get; private set; }

        public Vertex(T identifier, int x, int y, int width, int height)
        {
            //Contract.Requires<ArgumentNullException>(identifier != null, nameof(identifier));
            if (identifier == null) throw new ArgumentNullException(nameof(identifier));
            this.Identifier = identifier;
            this.X = x;
            this.Y = y;
            this.Width = width;
            this.Height = height;
        }

        public override bool Equals(object obj)
        {
            Vertex<T> vertex = obj as Vertex<T>;
            return vertex != null
                && this.Identifier.Equals(vertex.Identifier);
        }

        public override int GetHashCode()
        {
            return this.Identifier.GetHashCode();
        }
    }
}