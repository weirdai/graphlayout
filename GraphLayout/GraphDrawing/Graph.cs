﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GraphDrawing
{
    public class Graph<T>
    {
        private List<Vertex<T>> vertices = new List<Vertex<T>>;

        //private HashSet<Vertex<T>> vertices = new HashSet<Vertex<T>>();
        private Dictionary<Vertex<T>, Dictionary<Vertex<T>, Edge<T>>> edges = new Dictionary<Vertex<T>, Dictionary<Vertex<T>, Edge<T>>>();

        public IReadOnlyList<Vertex<T>> Vertices
        {
            get
            {
                return vertices.ToList().AsReadOnly();
            }
        }
        public IReadOnlyList<Edge<T>> Edges
        {
            get
            {
                return edges
                    .SelectMany(kvp => kvp.Value) //select all edges from a node
                    .Select(kvp => kvp.Value) // select the edge
                    .Distinct()
                    .ToList()
                    .AsReadOnly();
            }
        }

        public bool AddVertex(Vertex<T> vertex)
        {
            if (vertices.Add(vertex))
            {
                edges.Add(vertex, new Dictionary<Vertex<T>, Edge<T>>());
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AddEdge(Vertex<T> vertex1, Vertex<T> vertex2)
        {
            Edge<T> edge = new Edge<T>(vertex1, vertex2, 1);
            if (!edges[vertex1].ContainsKey(vertex2))
            {
                if (!edges[vertex2].ContainsKey(vertex1))
                {
                    edges[vertex1].Add(vertex2, edge);
                    edges[vertex2].Add(vertex1, edge);
                    return true;
                }
            }
            else if (edges[vertex2].ContainsKey(vertex1))
            {
                return false;
            }
            throw new InvalidOperationException("Edge collections out of sync!");
        }

        internal bool TryGetEdge(Vertex<T> vertex1, Vertex<T> vertex2, out Edge<T> edge)
        {
            if (edges.ContainsKey(vertex1)
                && edges[vertex1].ContainsKey(vertex2))
            {
                edge = edges[vertex1][vertex2];
                return true;
            }
            else
            {
                edge = null;
                return false;
            }
        }
    }
}