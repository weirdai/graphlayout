﻿namespace GraphDrawing
{
    internal interface IEnergyCalculator<T>
    {
        double GetEnergy(Graph<T> graph);
    }
}