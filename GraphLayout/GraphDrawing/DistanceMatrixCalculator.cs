﻿using MathNet.Numerics.LinearAlgebra.Double;

namespace GraphDrawing
{
    /// <summary>
    /// Floyd Marshall
    ///
    /// </summary>
    internal class DistanceMatrixCalculator<T>
    {
        private DenseMatrix GetDistanceMatrix(Graph<T> graph)
        {
            var vertices = graph.Vertices;
            var n = vertices.Count;
            DenseMatrix matrix = new DenseMatrix(n);
            InitializeToInfinity(n, matrix);
            var edges = graph.Edges;
            foreach (var edge in edges)
            {
            }

            return matrix;
        }

        private static void InitializeToInfinity(int n, DenseMatrix matrix)
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    matrix[i, j] = matrix[j, i] = double.PositiveInfinity;
                }
            }
        }
    }
}