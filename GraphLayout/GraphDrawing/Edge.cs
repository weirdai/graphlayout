﻿namespace GraphDrawing
{
    public class Edge<T>
    {
        public Vertex<T> Vertex1 { get; private set; }
        public Vertex<T> Vertex2 { get; private set; }
        public int Length { get; }

        public Edge(Vertex<T> vertex1, Vertex<T> vertex2, int length)
        {
            Vertex1 = vertex1;
            Vertex2 = vertex2;
            Length = length;
        }

        public override bool Equals(object obj)
        {
            Edge<T> edge = obj as Edge<T>;
            return edge != null &&
                (this.Vertex1.Equals(edge.Vertex1) &&
                this.Vertex2.Equals(edge.Vertex2) ||
                this.Vertex1.Equals(edge.Vertex2) &&
                this.Vertex2.Equals(edge.Vertex1));
        }

        public override int GetHashCode()
        {
            int vertex1HashCode = Vertex1.GetHashCode();
            int vertex2HashCode = Vertex2.GetHashCode();
            return (vertex1HashCode < vertex2HashCode)
                ? vertex1HashCode + 13 * vertex2HashCode
                : vertex2HashCode + 13 * vertex1HashCode;
        }
    }
}