﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GraphDrawing
{
    internal class SpringEnergyCalculator<T> : IEnergyCalculator<T>
    {
        // Kamada and Kawai chose alpha = 2, where as Cohen also considered alpha = 0 and alpha = 1.
        private readonly int alpha = 2;

        public double GetEnergy(Graph<T> graph)
        {
            return GetSpringEnergy(graph).Sum();
        }

        private IEnumerable<double> GetSpringEnergy(Graph<T> graph)
        {
            var vertices = graph.Vertices;
            for (int j = 0; j < vertices.Count; j++)
            {
                for (int i = j - 1; i >= 0; i--)
                {
                    yield return GetSingleSpringEnergy(graph, vertices[j], vertices[i]);
                }
            }
        }

        private double GetSingleSpringEnergy(Graph<T> graph, Vertex<T> vertex1, Vertex<T> vertex2)
        {
            var actualDistance = GetDistance(vertex1, vertex2); //||Xi - Xj||
            var desiredDistance = GetDesiredDistance(vertex1, vertex2, graph);
            var distanceDiff = actualDistance - desiredDistance;
            var normalizationConstant = Math.Pow(desiredDistance, -1 * alpha);
            return normalizationConstant * (distanceDiff * distanceDiff);
        }

        private double GetDesiredDistance(Vertex<T> vertex1, Vertex<T> vertex2, Graph<T> graph)
        {
            throw new NotImplementedException();
        }

        // ||Xi - Xj||
        private static double GetDistance(Vertex<T> vertex1, Vertex<T> vertex2)
        {
            var x1 = GetCenterX(vertex1);
            var y1 = GetCenterY(vertex1);
            var x2 = GetCenterX(vertex2);
            var y2 = GetCenterY(vertex2);

            var xDiff = x1 - x2;
            var yDiff = y1 - y2;

            return Math.Sqrt(xDiff * xDiff + yDiff * yDiff);
        }

        private static int GetCenterY(Vertex<T> vertex1)
        {
            return vertex1.Y + vertex1.Height / 2;
        }

        private static int GetCenterX(Vertex<T> vertex1)
        {
            return vertex1.X + vertex1.Width / 2;
        }
    }
}