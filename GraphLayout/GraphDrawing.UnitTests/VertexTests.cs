﻿using NUnit.Framework;
using Ploeh.AutoFixture;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GraphDrawing.UnitTests
{
    [TestFixture]
    public class VertexTests
    {
        [Test]
        public void TestConstructor()
        {
            var fixture = new Fixture();
            var x = fixture.Create<int>();
            var y = fixture.Create<int>();
            var width = fixture.Create<int>();
            var height = fixture.Create<int>();
            var identifier = fixture.Create<int>();
            var v = new Vertex<int>(identifier, x, y, width, height);
            Assert.IsNotNull(v);
            Assert.AreEqual(identifier, v.Identifier);
            Assert.AreEqual(x, v.X);
            Assert.AreEqual(y, v.Y);
            Assert.AreEqual(width, v.Width);
            Assert.AreEqual(height, v.Height);
        }

        [Test]
        public void TestNullInequality()
        {
            var fixture = new Fixture();
            Assert.IsFalse(fixture.Create<Vertex<int>>().Equals(null));
        }

        [Test]
        public void TestOtherClassInequality()
        {
            var fixture = new Fixture();
            Assert.IsFalse(fixture.Create<Vertex<int>>().Equals(fixture.Create<object>()));
        }

        [Test]
        public void TestSameIdentifierEquality()
        {
            var fixture = new Fixture();
            var vertex1a = fixture.Create<Vertex<int>>();
            var vertex1b = new Vertex<int>(vertex1a.Identifier, fixture.Create<int>(), fixture.Create<int>(), fixture.Create<int>(), fixture.Create<int>());
            Assert.IsTrue(vertex1a.Equals(vertex1b));
            Assert.IsTrue(vertex1b.Equals(vertex1a));
        }

        [Test]
        public void TestDifferentIdentifierInequality()
        {
            var fixture = new Fixture();
            var vertex1 = fixture.Create<Vertex<int>>();
            var vertex2 = new Vertex<int>(
                vertex1.Identifier + 1,
                fixture.Create<int>(),
                fixture.Create<int>(),
                fixture.Create<int>(),
                fixture.Create<int>());
            Assert.IsFalse(vertex1.Equals(vertex2));
            Assert.IsFalse(vertex2.Equals(vertex1));
        }

        [Test]
        public void TestSameIdentifierHashCode()
        {
            var fixture = new Fixture();
            var vertex1 = fixture.Create<Vertex<int>>();
            var vertex2 = new Vertex<int>(
                vertex1.Identifier,
                fixture.Create<int>(),
                fixture.Create<int>(),
                fixture.Create<int>(),
                fixture.Create<int>());
            Assert.AreEqual(vertex1.GetHashCode(), vertex2.GetHashCode());
        }
    }
}
