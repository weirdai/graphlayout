﻿using NUnit.Framework;
using System;
using GraphDrawing;
using System.Drawing;
using Ploeh.AutoFixture;

namespace GraphDrawing.UnitTests
{
    [TestFixture]
    public class GraphTest
    {
        [Test]
        public void TestConstructor()
        {
            Assert.IsNotNull(new Graph<int>());
        }

        [Test]
        public void TestAddVertex()
        {
            var fixture = new Fixture();
            var graph = new Graph<int>();
            var vertex = fixture.Create<Vertex<int>>();

            Assert.IsTrue(graph.AddVertex(vertex));
            Assert.IsFalse(graph.AddVertex(vertex));
        }

        [Test]
        public void TestAddEdge()
        {
            var fixture = new Fixture();
            var graph = new Graph<int>();
            Vertex<int> vertex1, vertex2;
            graph.AddVertex(vertex1 = fixture.Create<Vertex<int>>());
            while (!graph.AddVertex(vertex2 = fixture.Create<Vertex<int>>()));

            Assert.IsTrue(graph.AddEdge(vertex1, vertex2));
            Assert.IsFalse(graph.AddEdge(vertex1, vertex2));
            Assert.IsFalse(graph.AddEdge(vertex2, vertex1));
        }
    }
}
