﻿using NUnit.Framework;
using Ploeh.AutoFixture;

namespace GraphDrawing.UnitTests
{
    [TestFixture]
    public class EdgeTest
    {
        [Test]
        public void TestConstructor()
        {
            Fixture fixture = new Fixture();
            Assert.IsNotNull(new Edge<int>(fixture.Create<Vertex<int>>(), fixture.Create<Vertex<int>>(), fixture.Create<int>()));
        }

        [Test]
        public void TestGetVertices()
        {
            Fixture fixture = new Fixture();
            Vertex<int> vertex1 = fixture.Create<Vertex<int>>();
            Vertex<int> vertex2 = fixture.Create<Vertex<int>>();
            Edge<int> edge = new Edge<int>(vertex1, vertex2, fixture.Create<int>());
            Assert.IsTrue(edge.Vertex1.Equals(vertex1) && edge.Vertex2.Equals(vertex2) || edge.Vertex1.Equals(vertex2) && edge.Vertex2.Equals(vertex1));
        }

        [Test]
        public void TestNullInequality()
        {
            var fixture = new Fixture();
            Assert.IsFalse(fixture.Create<Edge<int>>().Equals(null));
        }

        [Test]
        public void TestOtherClassInequality()
        {
            var fixture = new Fixture();
            Assert.IsFalse(fixture.Create<Edge<int>>().Equals(fixture.Create<object>()));
        }

        [Test]
        public void TestSameVerticesSameOrderEquality()
        {
            var fixture = new Fixture();
            var edge1 = fixture.Create<Edge<int>>();
            var edge2 = new Edge<int>(edge1.Vertex1, edge1.Vertex2, fixture.Create<int>());
            Assert.IsTrue(edge1.Equals(edge2));
            Assert.IsTrue(edge2.Equals(edge1));
        }

        [Test]
        public void TestSameVerticesDifferentOrderEquality()
        {
            var fixture = new Fixture();
            var edge1 = fixture.Create<Edge<int>>();
            var edge2 = new Edge<int>(edge1.Vertex2, edge1.Vertex1, fixture.Create<int>());
            Assert.IsTrue(edge1.Equals(edge2));
            Assert.IsTrue(edge2.Equals(edge1));
        }

        [Test]
        public void TestDifferentIdentifierInequality()
        {
            var fixture = new Fixture();
            var edge1 = fixture.Create<Edge<int>>();
            Edge<int> edge2;
            do
            {
                edge2 = fixture.Create<Edge<int>>();
            } while (edge2.Vertex1.Equals(edge1.Vertex1) || edge2.Vertex1.Equals(edge1.Vertex2));

            Assert.IsFalse(edge1.Equals(edge2));
            Assert.IsFalse(edge2.Equals(edge1));
        }

        [Test]
        public void TestSameIdentifierHashCode()
        {
            var fixture = new Fixture();
            var edge1 = fixture.Create<Edge<int>>();
            var edge2 = new Edge<int>(edge1.Vertex2, edge1.Vertex1, fixture.Create<int>());
            Assert.AreEqual(edge1.GetHashCode(), edge2.GetHashCode());
            Assert.AreEqual(edge2.GetHashCode(), edge1.GetHashCode());
        }
    }
}