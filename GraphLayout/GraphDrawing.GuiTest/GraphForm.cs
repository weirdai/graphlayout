﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace GraphDrawing.GuiTest
{
    public partial class GraphForm : Form
    {
        private int minSize = 10;
        private Graph<int> graph = new Graph<int>();

        public GraphForm()
        {
            InitializeComponent();
            GenerateGraph(10, 20);
        }

        private void GenerateGraph(int vertices, int edges)
        {
            GenerateVertices(vertices);
            GenerateEdges(edges);
        }

        private void GenerateEdges(int edges)
        {
            Random r = new Random();
            IReadOnlyList<Vertex<int>> vertices = graph.Vertices;
            for (int i = 0; i < edges; i++)
            {
                Vertex<int> vertex1 = vertices[r.Next(0, vertices.Count)];
                Vertex<int> vertex2;
                do
                {
                    vertex2 = vertices[r.Next(0, vertices.Count)];
                } while (vertex1.Equals(vertex2));

                graph.AddEdge(vertex1, vertex2);
            }
        }

        private void GenerateVertices(int vertices)
        {
            Random r = new Random();
            int maxWidth = drawnGraphUserControl.Width / vertices;
            int maxHeight = drawnGraphUserControl.Height / vertices;
            for (int i = 0; i < vertices; i++)
            {
                Rectangle vertexLocation = GetRandomVertexRectangle(r, maxWidth, maxHeight);
                graph.AddVertex(new Vertex<int>(i, vertexLocation.X, vertexLocation.Y, vertexLocation.Width, vertexLocation.Height));
            }
        }

        private Rectangle GetRandomVertexRectangle(Random r, int maxWidth, int maxHeight)
        {
            Point location = new Point()
            {
                X = r.Next(0, drawnGraphUserControl.Width - minSize),
                Y = r.Next(0, drawnGraphUserControl.Height - minSize),
            };
            return new Rectangle()
            {
                Location = location,
                Size = new Size()
                {
                    Width = r.Next(minSize, maxWidth),
                    Height = r.Next(minSize, maxHeight)
                }
            };
        }
    }
}