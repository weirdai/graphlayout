﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GraphDrawing;

namespace GraphDrawing.GuiTest
{
    public partial class DrawnGraphUserControl<T> : UserControl
    {
        private Graph<T> graph;

        private DrawnGraphUserControl()
        {
            InitializeComponent();
        }

        public DrawnGraphUserControl(Graph<T> graph)
            : this()
        {
            this.graph = graph;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            int xOffset, yOffset;
            float zoomLevel;
            CalculateZoomAndOffsets(e.ClipRectangle, out xOffset, out yOffset, out zoomLevel);

            using (Pen linePen = new Pen(Brushes.Black))
            {
                foreach (Edge<T> edge in graph.Edges)
                {
                    e.Graphics.DrawLine(linePen,
                        GetCenterPoint(edge.Vertex1, xOffset, yOffset, zoomLevel),
                        GetCenterPoint(edge.Vertex2, xOffset, yOffset, zoomLevel));
                }
                foreach (Vertex<T> vertex in graph.Vertices)
                {
                    RectangleF scaledRectangle = GetScaledRectangle(vertex, xOffset, yOffset, zoomLevel);
                    e.Graphics.DrawRectangle(linePen, scaledRectangle.X, scaledRectangle.Y, scaledRectangle.Width, scaledRectangle.Height);
                }
            }
        }

        private RectangleF GetScaledRectangle(Vertex<T> vertex, int xOffset, int yOffset, float zoomLevel)
        {
            return new RectangleF()
            {
                X = (vertex.X - xOffset) * zoomLevel,
                Y = (vertex.Y - yOffset) * zoomLevel,
                Width = vertex.Width * zoomLevel,
                Height = vertex.Height * zoomLevel
            };
        }

        private PointF GetCenterPoint(Vertex<T> vertex1, int xOffset, int yOffset, float zoomLevel)
        {
            return new PointF()
            {
                X = (vertex1.X + vertex1.Width / 2f - xOffset) * zoomLevel,
                Y = (vertex1.Y + vertex1.Height / 2f - yOffset) * zoomLevel,
            };
        }

        private void CalculateZoomAndOffsets(Rectangle clipRectangle, out int xOffset, out int yOffset, out float zoomLevel)
        {
            xOffset = int.MaxValue;
            yOffset = int.MaxValue;
            int width = 0;
            int height = 0;
            foreach (Vertex<T> vertex in graph.Vertices)
            {
                if (vertex.X < xOffset)
                {
                    xOffset = vertex.X;
                }
                if (vertex.Y < yOffset)
                {
                    yOffset = vertex.Y;
                }
                if (width < vertex.X + vertex.Width)
                {
                    width = vertex.X + vertex.Width;
                }
                if (height < vertex.Y + vertex.Height)
                {
                    height = vertex.Y + vertex.Height;
                }
            }
            float xZoom = clipRectangle.Width / (float)(width - xOffset);
            float yZoom = clipRectangle.Height / (float)(height - yOffset);
            zoomLevel = Math.Min(xZoom, yZoom);
        }

    }
}
