﻿namespace GraphDrawing.GuiTest
{
    partial class GraphForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonPanel = new System.Windows.Forms.Panel();
            this.drawnGraphUserControl = new GraphDrawing.GuiTest.DrawnGraphUserControl<int>(this.graph);
            this.SuspendLayout();
            // 
            // buttonPanel
            // 
            this.buttonPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.buttonPanel.Location = new System.Drawing.Point(0, 0);
            this.buttonPanel.Name = "buttonPanel";
            this.buttonPanel.Size = new System.Drawing.Size(854, 100);
            this.buttonPanel.TabIndex = 0;
            // 
            // drawnGraphUserControl
            // 
            this.drawnGraphUserControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.drawnGraphUserControl.Location = new System.Drawing.Point(0, 100);
            this.drawnGraphUserControl.Name = "drawnGraphUserControl";
            this.drawnGraphUserControl.Size = new System.Drawing.Size(854, 623);
            this.drawnGraphUserControl.TabIndex = 1;
            // 
            // GraphForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(854, 723);
            this.Controls.Add(this.drawnGraphUserControl);
            this.Controls.Add(this.buttonPanel);
            this.Name = "GraphForm";
            this.Text = "Graphs";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel buttonPanel;
        private DrawnGraphUserControl<int> drawnGraphUserControl;
    }
}

